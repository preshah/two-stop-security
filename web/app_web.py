from flask import Flask, redirect, render_template, jsonify, request


app = Flask(__name__)


ACCEPTABLE_TOKENS = ['55abc']


@app.route('/<token>/index.htm')
def index(token):
    if token in ACCEPTABLE_TOKENS:
        return render_template('index.html')
    else:
        return render_template('redirect.html')


@app.route('/redirect_login',  methods=["GET"])
def redirect_login():
    if request.method == "GET":
        return redirect('http://localhost/login', code=302)


@app.errorhandler(404)
# inbuilt function which takes error as parameter
def not_found(e):
    # defining function
    return jsonify({'errorCode': 404, 'message': 'Route not found'})


if __name__ == '__main__':
    # app.run('0.0.0.0', debug=True, port=8081, ssl_context=('mycert.crt', 'mykey.key'))
    app.run('0.0.0.0', debug=True, port=8081)
