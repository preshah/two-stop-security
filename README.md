# two-stop-security

Two stop security is exactly as it sounds, an app designed to provide access to a top secret page by the following:

- Authenticating an end user
- Providing a token which will be required to access the secret page over a secured link. 


### Setup

Following are the commands required to be executed to bring up the application:

- `docker-compose build` (Builds the required container images)
- `docker-compose up` (Run the containers required for the application)

### Usage

Once the application is up and running, go to the following url on your browser:

- https://localhost/login
- Enter Login username `preyansh` and password `Test@123`
- Once you are logged in, you will be taken to your profile. 
- The token is set upon successful login and added to the url for accessing the secret page. Also the web page will is secured over TLS.
- Click on the access link to go to the top secret page.

### Teardown
- Please execute a `docker-compose down` in order to tear down the setup

### Documentation (Additional Info)

- Find the application related details here: https://redlock.atlassian.net/wiki/spaces/~582997714/pages/2777056269/Two+Stop+Security
