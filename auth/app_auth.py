from flask import Flask, flash, request, redirect, render_template, jsonify


app = Flask(__name__)
app.config['SECRET_KEY'] = 'super secret key'

#SITE_NAME = 'https://web:8081'


USERS_PERMITTED = {
        "preyansh": {"password": "Test@123", "token": "55abc"}
}


@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        name = request.form['username']
        password = request.form['password']
        if name not in USERS_PERMITTED.keys() and password != USERS_PERMITTED[name]:
            error = 'Invalid credentials'
        else:
            #flash('You were successfully logged in')
            return redirect(f"/login/user/{name}")
    return render_template('login.html', error=error)


@app.route('/login/user/<name>')
def user(name):
    url = 'http://localhost/{}/index.htm'.format(USERS_PERMITTED[name]['token'])
    return render_template('user.html', name=name, url=url)


@app.errorhandler(404)
# inbuilt function which takes error as parameter
def not_found(e):
    # defining function
    return jsonify({'errorCode': 404, 'message': 'Route not found'})


if __name__ == "__main__":
    app.run('0.0.0.0', debug=True, port=8082)
